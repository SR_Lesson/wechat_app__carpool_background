package com.wx.carcode.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.wx.carcode.entity.WxUser;
import com.wx.carcode.service.WxService;
import com.wx.carcode.utils.SendMail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

@Service
public class WxServiceImpl implements WxService {

    private Logger log= LoggerFactory.getLogger(WxServiceImpl.class);


    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private SendMail sendMail;

    @Value(value = "${appId}")
    private String appId;
    @Value(value = "${secretId}")
    private String secretId;
    @Value(value = "${url}")
    private String url;

    /*
    根据code后台获取openId
     */
    @Override
    public JSONObject getOpenId(String code) {
        String requestUrl = url+"?appid="+appId+"&secret="+secretId+"&js_code="+code+"&grant_type=authorization_code";
        String result = restTemplate.getForObject(requestUrl,String.class);
        log.info("openid:"+result);
        return JSONObject.parseObject(result);
    }

    @Override
    public void sendMessage(String message, String userName) {

        sendMail.sendSimpleMail(message,userName);
    }


}
