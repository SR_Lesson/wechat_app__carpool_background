package com.wx.carcode.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.wx.carcode.entity.WxUser;
import com.wx.carcode.mapper.WxUserMapper;
import com.wx.carcode.service.WxUserService;
import com.wx.carcode.utils.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;


@Service
public class WxUserServiceImpl implements WxUserService {

    @Resource
    private WxUserMapper wxUserMapper;

    @Override
    public JSONObject insert(WxUser user) {
        SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        String loginDate = format.format(new Date());
        user.setLoginDate(loginDate);
        int change = wxUserMapper.insert(user);
        return JsonUtil.updateJson(change);
    }
    // 查找人
    @Override
    public WxUser selectByOpenId(String userId) {
        return wxUserMapper.selectByOpenId(userId);
    }

    @Override
    public JSONObject update(WxUser user) {
        int change = wxUserMapper.update(user);
        return JsonUtil.updateJson(change);
    }
}
