package com.wx.carcode.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wx.carcode.entity.Release;
import com.wx.carcode.mapper.ReleaseMapper;
import com.wx.carcode.service.ReleaseService;
import com.wx.carcode.utils.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class ReleaseServiceImpl implements ReleaseService {

    @Autowired(required = false)
    private ReleaseMapper releaseMapper;

    //发布新消息
    @Override
    public JSONObject addRelease(Release release) {
        SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        String addDate = format.format(new Date());
        release.setReleaseDate(addDate);
        int change = releaseMapper.insert(release);
        return JsonUtil.updateJson(change);
    }
    //查询消息
    @Override
    public PageInfo<Release> selectRelease(Release release) {
        PageHelper.startPage(release.getPageNum(),release.getPageSize());
        List<Release> list = releaseMapper.select(release);
        return new PageInfo<>(list);
    }
    //查找消息
    @Override
    public List<Map<Object, Object>> findCount(String userId) {
        return releaseMapper.findCount(userId);
    }
    //删除消息
    @Override
    public JSONObject delete(Integer id) {
        int change=releaseMapper.delete(id);
        return JsonUtil.updateJson(change);
    }
}
