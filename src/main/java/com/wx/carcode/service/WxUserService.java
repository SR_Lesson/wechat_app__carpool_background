package com.wx.carcode.service;

import com.alibaba.fastjson.JSONObject;
import com.wx.carcode.entity.WxUser;

public interface WxUserService {

    JSONObject insert(WxUser user);

    WxUser selectByOpenId(String userId);

    JSONObject update(WxUser user);
}
