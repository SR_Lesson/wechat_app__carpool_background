package com.wx.carcode.service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.wx.carcode.entity.Release;

import java.util.List;
import java.util.Map;

public interface ReleaseService {
    JSONObject addRelease(Release release);

    PageInfo<Release> selectRelease(Release release);

    List<Map<Object, Object>> findCount(String userId);

    JSONObject delete(Integer id);
}
