package com.wx.carcode.service;

import com.alibaba.fastjson.JSONObject;
import com.wx.carcode.utils.GetOpenId;
import com.wx.carcode.utils.JsonUtil;
import com.wx.carcode.utils.SendMail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



public interface WxService {

    JSONObject getOpenId(String code);

    void sendMessage(String message, String userName);
}
