package com.wx.carcode.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.wx.carcode.entity.Release;
import com.wx.carcode.entity.WxUser;
import com.wx.carcode.service.ReleaseService;
import com.wx.carcode.service.WxService;
import com.wx.carcode.service.WxUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/wx")
public class WxController {


    @Autowired
    private WxService wxService;
    @Autowired
    private WxUserService wxUserService;
    @Autowired
    private ReleaseService releaseService;

    /**
     * 根据用户的授权信息，获取的code ，后台获取opendId
     * @param code
     * @return
     */
    @GetMapping("/getOpenId")
    public JSONObject getOpenId(String code){

        return wxService.getOpenId(code);
    }

    /*
   用户信息添加
    */
    @PostMapping("/loginInfo")
    public JSONObject loginInfo(@RequestBody WxUser user){
        log.info("userName:"+user.getUserName());
        return wxUserService.insert(user);
    }

    /*
    发布消息
     */
    @PostMapping("/addRelease")
    public JSONObject addRelease(@RequestBody Release release){

        return releaseService.addRelease(release);
    }

    @PostMapping("/selectRelease")
    public PageInfo<Release> selectRelease(@RequestBody Release release){
        return releaseService.selectRelease(release);
    }



    @GetMapping("/findCount")
    public List<Map<Object,Object>> findCount(String userId){
        return releaseService.findCount(userId);
    }

    @PostMapping("/deleteInfo")
    public JSONObject deleteInfo(@RequestBody Map<String,Integer> map){
        Integer id = map.get("id");
        return releaseService.delete(id);
    }

    @PostMapping("/getUserInfo")
    public WxUser getUserInfo(@RequestBody Map<String,String>map){
        String userId = map.get("openId");
        return wxUserService.selectByOpenId(userId);
    }

    @PostMapping("/updateInfo")
    public JSONObject updateInfo(@RequestBody WxUser user){
        return wxUserService.update(user);
    }

    @PostMapping("/sendMessage")
    public void sendMessage(@RequestBody Map<String,String>map){
        String message = map.get("message");
        String userName = map.get("userName");
        wxService.sendMessage(message,userName);
    }


}
