package com.wx.carcode;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarcodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarcodeApplication.class, args);
    }

}
