package com.wx.carcode.mapper;

import com.wx.carcode.entity.Release;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
public interface ReleaseMapper {
    int insert(@Param("pojo") Release pojo);

    List<Release> select(@Param("pojo") Release pojo);

    List<Map<Object,Object>> findCount(@Param("userId") String userId);

    int delete(@Param("id") Integer id);
}
