package com.wx.carcode.mapper;

import com.wx.carcode.entity.WxUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
public interface WxUserMapper {

    int insert(@Param("pojo") WxUser pojo);

    int insertList(@Param("pojos") List<WxUser> pojo);

    List<WxUser> select(@Param("pojo") WxUser pojo);

    int update(@Param("pojo") WxUser pojo);

    WxUser selectByOpenId(@Param("userId") String userId);

}
