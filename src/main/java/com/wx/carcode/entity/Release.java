package com.wx.carcode.entity;

import lombok.Data;


import java.util.List;

/**
 * 发布消息
 */
@Data
public class Release {
    private Integer id;
    private String userId;
    private String startAddress;
    private String endAddress;
    private String goDate;
    private String tel;
    private String peopleCount;
    private String mark;
    private String releaseDate;
    private Boolean isShow;
    private String ftype;
    private Integer pageNum;
    private Integer pageSize;
    private List<WxUser> list;

}
